#!/usr/bin/env python
#! -*- coding: utf-8 -*-

import os
import SimpleITK as sitk
import json

import logging as log

from Compartments import Compartment
from tools import get_sitk_image


def get_mask(voxel_value, masks):
    masks = get_sitk_image(masks)

    outside = 0; inside = 1
    bth = sitk.BinaryThresholdImageFilter()
    bth.SetLowerThreshold(voxel_value); bth.SetUpperThreshold(voxel_value)
    bth.SetInsideValue(inside); bth.SetOutsideValue(outside)
    mask = bth.Execute(masks)
    
    return mask


def get_all_info(image_path, masks_path, res_dc=dict(), save_images_root=False):
    if save_images_root:
        log.info("Saving Segmentations to {}".format(os.path.abspath(save_images_root)))

    params_to_save = ('Area_yz_cm2', 'Volume_cm3', 'ThLower', 'ThUpper', 'N_Voxel', 'PixelArea_yz_cm2', 'VoxelVolume_cm3')
    #Naming scheme for results dictionary with IMAGE_MASKS
    the_name = "{}_{}".format(os.path.split(image_path)[-1].split(".")[0], 
                               os.path.split(masks_path)[-1].split(".")[0])
    if the_name in res_dc.keys():
        log.warning("values from Image {} and Mask {} are already in results dictionary -> overwriting...".format(image_path, masks_path))

    res_dc[the_name] = {"ImagePath": os.path.abspath(image_path),
                        "MasksPath": os.path.abspath(masks_path)}

    SubcutaneousCompartment = get_mask(voxel_value=1, masks=masks_path) 
    SubcutaneousCompartment = Compartment(image_path, SubcutaneousCompartment)
    SubcutaneousCompartment.add_fat_properties(opening_size_mm=1.) #remove single voxels from volume effect at the skin
    res_dc[the_name]["SubcutaneousCompartment"] = dict()
    res_dc[the_name]["SubcutaneousCompartment"]["Complete"] = {k: SubcutaneousCompartment.Complete[k] for k in params_to_save}
    res_dc[the_name]["SubcutaneousCompartment"]["Fat"] = {k: SubcutaneousCompartment.Fat[k] for k in params_to_save}
    if save_images_root:
        sitk.WriteImage(SubcutaneousCompartment.Fat.Seg, os.path.join(save_images_root, the_name + "_SubcutaneousCompartment_Fat.nii.gz"))
        sitk.WriteImage(SubcutaneousCompartment.Complete.Seg, os.path.join(save_images_root, the_name + "_SubcutaneousCompartment_Complete.nii.gz"))

    MuscleCompartment = get_mask(voxel_value=2, masks=masks_path)
    MuscleCompartment = Compartment(image_path, MuscleCompartment)
    MuscleCompartment.add_fat_properties() #opening needed??
    MuscleCompartment.add_muscle_properties()
    res_dc[the_name]["MuscleCompartment"] = dict()
    res_dc[the_name]["MuscleCompartment"]["Complete"] = {k: MuscleCompartment.Complete[k] for k in params_to_save}
    res_dc[the_name]["MuscleCompartment"]["Fat"] = {k: MuscleCompartment.Fat[k] for k in params_to_save}
    res_dc[the_name]["MuscleCompartment"]["Muscle"] = {k: MuscleCompartment.Muscle[k] for k in params_to_save}
    if save_images_root:
        sitk.WriteImage(MuscleCompartment.Fat.Seg, os.path.join(save_images_root, the_name + "_MuscleCompartment_Fat.nii.gz"))
        sitk.WriteImage(MuscleCompartment.Muscle.Seg, os.path.join(save_images_root, the_name + "_MuscleCompartment_Muscle.nii.gz"))
        sitk.WriteImage(MuscleCompartment.Complete.Seg, os.path.join(save_images_root, the_name + "_MuscleCompartment_Complete.nii.gz"))

    VisceralCompartment = get_mask(voxel_value=3, masks=masks_path)
    VisceralCompartment = Compartment(image_path, VisceralCompartment)
    VisceralCompartment.add_fat_properties(opening_size_mm=1.) #remove single voxels from volume effect of gas in digestive tract
    res_dc[the_name]["VisceralCompartment"] = dict()
    res_dc[the_name]["VisceralCompartment"]["Complete"] = {k: VisceralCompartment.Complete[k] for k in params_to_save}
    res_dc[the_name]["VisceralCompartment"]["Fat"] = {k: VisceralCompartment.Fat[k] for k in params_to_save}
    if save_images_root:
        sitk.WriteImage(VisceralCompartment.Fat.Seg, os.path.join(save_images_root, the_name + "_VisceralCompartment_Fat.nii.gz"))
        sitk.WriteImage(VisceralCompartment.Complete.Seg, os.path.join(save_images_root, the_name + "_VisceralCompartment_Complete.nii.gz"))

    log.info("Important results for the report are: \n \
              Visceral fat area (VFA) in cm^2: {} \n \
              Subcutaneous fat area (SFA) in cm^2: {} \n \
              VFA / SFA: {} \n \
              Muscle area (MA) in cm^2: {} \n \
              Intramuscular adipose tissue (IMAT) in cm^2: {}".format(VisceralCompartment.Fat.Area_yz_cm2,
                                                                      SubcutaneousCompartment.Fat.Area_yz_cm2,
                                                                      VisceralCompartment.Fat.Area_yz_cm2 / SubcutaneousCompartment.Fat.Area_yz_cm2,
                                                                      MuscleCompartment.Muscle.Area_yz_cm2,
                                                                      MuscleCompartment.Fat.Area_yz_cm2))

    return res_dc


if __name__ == "__main__":
    loglevel = log.DEBUG
    log.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=loglevel)
    masks_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat_training_res_0/cv_niftis_postprocessed/8320540_pic.nii.gz"
    image_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat_training_res_0/imagesTr/8320540_pic_0000.nii.gz"
    results_dict = get_all_info(image_path, masks_path, save_images_root='.') 

    masks_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat_training_res_0/cv_niftis_postprocessed/8339332_pic.nii.gz"
    image_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat_training_res_0/imagesTr/8339332_pic_0000.nii.gz"
    results_dict = get_all_info(image_path, masks_path, res_dc=results_dict, save_images_root='.') 

    with open("results.json", "w") as f:
        json.dump(results_dict, f)

    # for further analysis (and nice pics), all stored masks and the final results json file should be kept
