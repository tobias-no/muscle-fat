#!/usr/bin/env python
#! -*- coding: utf-8 -*-

# CAVE: This lookup table is not valid for transitional or assimilated vertebrae

vertebra_lut = {(1,2): {'Name': 'C1-C2', 'Slice':None},
                (2,3): {'Name': 'C2-C3', 'Slice':None},
                (3,4): {'Name': 'C3-C4', 'Slice':None},
                (4,5): {'Name': 'C4-C5', 'Slice':None},
                (5,6): {'Name': 'C5-C6', 'Slice':None},
                (6,7): {'Name': 'C6-C7', 'Slice':None},
                (7,8): {'Name': 'C7-B1', 'Slice':None},
                (8,9): {'Name': 'B1-B2', 'Slice':None},
                (9,10): {'Name': 'B2-B3', 'Slice':None},
                (10,11): {'Name': 'B3-B4', 'Slice':None},
                (11,12): {'Name': 'B4-B5', 'Slice':None},
                (12,13): {'Name': 'B5-B6', 'Slice':None},
                (13,14): {'Name': 'B6-B7', 'Slice':None},
                (14,15): {'Name': 'B7-B8', 'Slice':None},
                (15,16): {'Name': 'B8-B9', 'Slice':None},
                (16,17): {'Name': 'B9-B10', 'Slice':None},
                (17,18): {'Name': 'B10-B11', 'Slice':None},
                (18,19): {'Name': 'B11-B12', 'Slice':None},
                (19,20): {'Name': 'B12-L1', 'Slice':None},
                (20,21): {'Name': 'L1-L2', 'Slice':None},
                (21,22): {'Name': 'L2-L3', 'Slice':None},
                (22,23): {'Name': 'L3-L4', 'Slice':None},
                (23,24): {'Name': 'L4-L5', 'Slice':None},
                (24,25): {'Name': 'L5-S1', 'Slice':None}}
