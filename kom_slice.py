#!/usr/bin/env python
#! -*- coding: utf-8 -*-

import os
import sys
import SimpleITK as sitk
import numpy as np
import json

import logging as log

from tools import get_sitk_image
import vertebra_lut


def extract_slice(img, z):
    img = get_sitk_image(img)
    x_size, y_size, z_size = img.GetSize()

    return sitk.Extract(img, [x_size, y_size, 1], [0, 0, z])


class ZSliceSelector(object):
    def __init__(self, masks):
        self.Masks = get_sitk_image(masks)
        self.vertebra_lut = vertebra_lut.vertebra_lut
    
    def get_slices(self):
        # loop through all slices to find the direction of CT-scan
        # Slices are defined by the upper boundaries of a vertebra,
        # since the processi spinosi reach further down

        # there seem to be some errors for vertebra classification at the boundaries of a scan volume
        n_voxels_vertebra_error_threshold = 30 

        max_vertebra_slices = []
        for z in range(0, self.Masks.GetSize()[2]):
            s_array = sitk.GetArrayFromImage(extract_slice(self.Masks, z))
            s_vertebra_indices = np.array(np.unique(s_array, return_counts=True))
            s_vertebra_indices = s_vertebra_indices[0, s_vertebra_indices[1] > n_voxels_vertebra_error_threshold] #remove error classification
            max_vertebra_slices.append(np.max(s_vertebra_indices))

        # beware of the direction!!!
        # z-direction from bottom to top z=0: large vertebra number -> z=last: low vertebra number
        #if max_vertebra_slices[1] > max_vertebra_slices[-2]: #perhaps better because of errors in vertebra classification at boundaries of scan volume
        if max_vertebra_slices[0] > max_vertebra_slices[-1]: #perhaps [-2] is a better idea because of errors in vertebra classification
            for i, vertebra in enumerate(max_vertebra_slices[:-1]):
                #print(i, vertebra)
                if vertebra != max_vertebra_slices[i+1]:
                    #print("blub", i, vertebra)
                    self.vertebra_lut[(vertebra-1,vertebra)]["Slice"] = i

        # z-direction from top to bottom z=last: large vertebra number -> z=0: low vertebra number
        #elif max_vertebra_slices[1] < max_vertebra_slices[-2]: #perhaps better because of errors in vertebra classification at boundaries  of scan volume
        elif max_vertebra_slices[0] < max_vertebra_slices[-1]: 
            for i, vertebra in enumerate(max_vertebra_slices[:-1]):
                #print(i, vertebra)
                if vertebra != max_vertebra_slices[i+1]:
                    #print("blub", i, vertebra)
                    self.vertebra_lut[(vertebra,vertebra+1)]["Slice"] = i + 1
        else:
            log.critical("Vertebrae detection seems to have failed.")

        # for better readability and possibility to save as json
        self.vertebra_lut = self.remap_dict(self.vertebra_lut)

    # method only for this use case to have better readability for access in lut values
    def remap_dict(self, input_dict):
        for k, v in input_dict.items():
            input_dict[k]["Indices"] = k
        return {input_dict[k]["Name"]: v for k, v in input_dict.items()}


def reorientation(original_image):
    original_image = get_sitk_image(original_image)
    x_spacing, y_spacing, z_spacing = original_image.GetSpacing()
    original_image.SetSpacing((x_spacing, y_spacing, 2.))

    a = sitk.GetArrayFromImage(original_image)

    b = np.empty((a.shape[2], 1, a.shape[1]))
    for i in range(0, a.shape[1]):
        b[:,0,i] = a[0,i,:]

    new_image = sitk.GetImageFromArray(b, isVector=False)
    new_image.SetSpacing([y_spacing, 2., x_spacing])
    x_origin, y_origin, z_origin = original_image.GetOrigin()
    new_image.SetOrigin((y_origin, z_origin, x_origin))

    return new_image

if __name__ == "__main__":
    loglevel = log.DEBUG
    log.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=loglevel)
    image_path = "/media/tn/6AE85391E8535A81/komorbidom/spine/1034_30590000_0000.nii.gz"
    masks_path = "/media/tn/6AE85391E8535A81/komorbidom/spine/1034_30590000.nii.gz"
    save_image_root = "."
    save_name = os.path.split(image_path)[-1].split(".")[0]
    save_json = save_name + "_v_lut.json"

    slices = ZSliceSelector(masks_path)
    slices.get_slices()

    # should be kept for further analysis
    log.info("Saving results of Look up table in {}".format(save_json))
    with open(save_json, "w") as f: 
        json.dump(slices.vertebra_lut, f)

    if slices.vertebra_lut['L2-L3']["Slice"] == None:
        log.error("{} not in scan volume".format(slices.vertebra_lut['L2-L3']["Name"]))
    else:
        slice_img = extract_slice(image_path, slices.vertebra_lut['L2-L3']["Slice"])

        #make orientation according to example files ?!
        slice_img = reorientation(slice_img)

        # should also be kept for further analysis (and nice pics)
        log.info("Saving slice of L2-L3 at z={} to {}".format(slices.vertebra_lut['L2-L3']["Slice"], os.path.join(save_image_root, save_name + "_" +str(slices.vertebra_lut['L2-L3']["Slice"])+"_L23.nii.gz")))
        sitk.WriteImage(slice_img, os.path.join(save_image_root, save_name + "_" + str(slices.vertebra_lut['L2-L3']["Slice"]) + "_L23.nii.gz"))




