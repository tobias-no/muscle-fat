#!/usr/bin/env python
#! -*- coding: utf-8 -*-

import sys
import SimpleITK as sitk

# make dictionary values accessible by attributes similar to pandas
class DictAccessByAttr(dict):
    def __init__(self, *args, **kwargs):
        super(DictAccessByAttr, self).__init__(*args, **kwargs)
        self.__dict__ = self

def get_sitk_image(img):
    if isinstance(img, sitk.Image):
        return img
    else:
        try:
            img = sitk.ReadImage(img)
            return img
        except BaseException as e:
            print("{} not existing or not a valid sitk image object: {}".format(img, e))
            sys.exit(-1)

