#!/usr/bin/env python
#! -*- coding: utf-8 -*-

import numpy as np
import SimpleITK as sitk

from tools import DictAccessByAttr, get_sitk_image

def execute_opening(mask, opening_size_mm):
    r_voxel_continous = opening_size_mm / mask.GetSpacing()[1]
    r_voxel = int(np.rint(r_voxel_continous))
    #print("opening is activated with {}mm: {} Voxel (Spacing: {})".format(opening_size_mm, r_voxel, mask.GetSpacing()))

    #there seems to be no difference for 2D operation and 3D operation on 2D image
    #so for consistensy 3D is used since in specific komorbidom case axial images are used sagitally
    mask = sitk.BinaryMorphologicalOpening(mask, [r_voxel]*3)

    return mask

class Compartment(object):
    def __init__(self, img, compartment_mask):
        self.Complete = self.basic_properties(compartment_mask)
        self.Image = get_sitk_image(img)

    def basic_properties(self, mask, th_lower=None, th_upper=None):
        p = DictAccessByAttr()

        p.Seg = get_sitk_image(mask)
        p.Spacing = p.Seg.GetSpacing()
        p.VoxelVolume_cm3 = p.Seg.GetSpacing()[0] * p.Seg.GetSpacing()[1] * p.Seg.GetSpacing()[2] / 1000.  # cm3
        p.PixelArea_xy_cm2 = p.Seg.GetSpacing()[0] * p.Seg.GetSpacing()[1] / 100. #cm2
        p.PixelArea_yz_cm2 = p.Seg.GetSpacing()[1] * p.Seg.GetSpacing()[2] / 100.

        p.ThLower = th_lower
        p.ThUpper = th_upper

        nda = sitk.GetArrayFromImage(p.Seg)

        p.N_Voxel = nda[nda[:]>0].size
        p.Volume_cm3 = p.N_Voxel * p.VoxelVolume_cm3 #cm3
        p.Area_cm2 = p.N_Voxel * p.PixelArea_xy_cm2 #cm2
        p.Area_yz_cm2 = p.N_Voxel * p.PixelArea_yz_cm2

        return p

    def add_fat_properties(self, opening_size_mm=0):
        th_lower = -190
        th_upper = -30
        fat_mask_3d = self.get_mask_3d(th_lower, th_upper)
        fat_mask = sitk.MaskImageFilter().Execute(fat_mask_3d, self.Complete.Seg)
        if opening_size_mm:
           fat_mask = execute_opening(fat_mask, opening_size_mm)
        self.Fat = self.basic_properties(mask=fat_mask,
                                         th_lower=th_lower, 
                                         th_upper=th_upper)

    def add_muscle_properties(self, opening_size_mm=0):
        th_lower = -29
        th_upper = 150
        muscle_mask_3d = self.get_mask_3d(th_lower, th_upper)
        muscle_mask = sitk.MaskImageFilter().Execute(muscle_mask_3d, self.Complete.Seg)
        if opening_size_mm:
           muscle_mask = execute_opening(muscle_mask, opening_size_mm)
        self.Muscle = self.basic_properties(mask = muscle_mask,
                                            th_lower=th_lower, 
                                            th_upper=th_upper)

    def get_mask_3d(self, th_lower, th_upper, outside=0, inside=1):
        outside = outside
        inside = inside
        bth = sitk.BinaryThresholdImageFilter()
        bth.SetLowerThreshold(th_lower)
        bth.SetUpperThreshold(th_upper)
        bth.SetInsideValue(inside)
        bth.SetOutsideValue(outside)
        return bth.Execute(self.Image)


if __name__ == '__main__':
    image_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat/tavi2020_pic.nrrd"
    muscle_compartment_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat/tavi2020_l23_muscle.nrrd"
    visceral_compartment_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat/tavi2020_l23_viscerale.nrrd"
    subcutaneous_compartment_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat/tavi2020_l23_subcutaneous.nrrd"

    image_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat/copd_pic.nrrd"
    muscle_compartment_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat/copd_l23_muscle.nrrd"
    visceral_compartment_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat/copd_l23_viscerale.nrrd"
    subcutaneous_compartment_path = "/media/tn/6AE85391E8535A81/komorbidom/vatsat/copd_l23_subcutaneous.nrrd"

    muscle_c = Compartment(image_path, muscle_compartment_path)
    muscle_c.add_fat_properties()
    muscle_c.add_muscle_properties()
    print("muscle complete")
    print(muscle_c.Complete.items())
    print("muscle fat")
    print(muscle_c.Fat.items())
    print("muscle muscle")
    print(muscle_c.Muscle.items())

    sat_c = Compartment(image_path, subcutaneous_compartment_path)
    sat_c.add_fat_properties()
    print("sat complete")
    print(sat_c.Complete.items())
    print("sat fat")
    print(sat_c.Fat.items())

    vat_c = Compartment(image_path, visceral_compartment_path)
    vat_c.add_fat_properties()
    print("vat complete")
    print(vat_c.Complete.items())
    print("vat fat")
    print(vat_c.Fat.items())




